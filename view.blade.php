<!-- Start Header07 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A nav component -->
@endif
<div class="header-07" is="fir-header-07" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
    <div class="header-07__wrap">
        <a href="/" class="header-07__logo">
            Return Home
        </a>

        @if($nav_menu)
        {{ Fir\Utils\Helpers::getNav($nav_menu, 'header-07') }}    
        @endif

        <ul class="header-07__list header-07__list--social">

            @if($instagram ?? false)
            <li>
                <a href="{{ $instagram }}">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
                    aria-describedby="desc" role="img" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <title>Instagram</title>
                    <desc>Instagram Logo in solid color</desc>
                      <path data-name="layer1"
                    d="M8.2 0h47.6A7.989 7.989 0 0 1 64 8.2v47.6a7.989 7.989 0 0 1-8.2 8.2H8.2A7.989 7.989 0 0 1 0 55.8V8.2A7.989 7.989 0 0 1 8.2 0zm38.4 7.1a2.9 2.9 0 0 0-2.9 2.9v6.9a2.9 2.9 0 0 0 2.9 2.9h7.2a2.9 2.9 0 0 0 2.9-2.9V10a2.9 2.9 0 0 0-2.9-2.9zm10.2 20h-5.6a19.758 19.758 0 0 1 .8 5.5c0 10.6-8.9 19.3-19.9 19.3s-19.9-8.6-19.9-19.3a19.758 19.758 0 0 1 .8-5.5H7.1v27a2.476 2.476 0 0 0 2.5 2.5h44.6a2.476 2.476 0 0 0 2.5-2.5l.1-27zm-24.7-7.7a12.723 12.723 0 0 0-12.9 12.5 12.64 12.64 0 0 0 12.9 12.4A12.723 12.723 0 0 0 45 31.8a12.64 12.64 0 0 0-12.9-12.4z"
                    fill="#202020"></path>
                    </svg>
                </a>
            </li>
            @endif

            @if($facebook ?? false)
            <li>
                <a href="{{ $facebook }}">
                    Facebook
                </a>
            </li>
            @endif

            @if($youtube ?? false)
            <li>
                <a href="{{ $youtube }}">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
                    aria-describedby="desc" role="img" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <title>Youtube</title>
                      <desc>YouTube logo in solid color.</desc>
                      <path data-name="layer1"
                      d="M50.708 9H13.292A13.25 13.25 0 0 0 0 22.207v18.586A13.25 13.25 0 0 0 13.292 54h37.416A13.25 13.25 0 0 0 64 40.793V22.207A13.25 13.25 0 0 0 50.708 9zm-8.989 23.4l-17.501 8.3a.7.7 0 0 1-1.005-.63V22.962a.7.7 0 0 1 1.02-.623l17.5 8.812a.7.7 0 0 1-.015 1.253zm0 0"
                      fill="#202020"></path>
                    </svg>
                </a>
            </li>
            @endif

            @if($tiktok ?? false)
            <li>
                <a href="{{ $tiktok }}">
                    TikTok
                </a>
            </li>
            @endif

            @if($snapchat ?? false)
            <li>
                <a href="{{ $snapchat }}">
                    Snapchat
                </a>
            </li>
            @endif

            @if($twitter ?? false)
            <li>
                <a href="{{ $twitter }}">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
                    aria-describedby="desc" role="img" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <title>Twitter</title>
                      <desc>Twitter Logo in solid color.</desc>
                      <path data-name="layer1"
                      d="M64 13.194a23.1 23.1 0 0 1-7.3 2.1 14.119 14.119 0 0 0 5.5-7.2c-1.9 1.2-6.1 2.9-8.2 2.9a13.782 13.782 0 0 0-9.6-4 13.187 13.187 0 0 0-13.2 13.2 13.576 13.576 0 0 0 .3 2.9c-9.9-.3-21.5-5.2-28-13.7a13.206 13.206 0 0 0 4 17.4c-1.5.2-4.4-.1-5.7-1.4-.1 4.6 2.1 10.7 10.2 12.9-1.6.8-4.3.6-5.5.4.4 3.9 5.9 9 11.8 9-2.1 2.4-9.3 6.9-18.3 5.5a39.825 39.825 0 0 0 20.7 5.8 36.8 36.8 0 0 0 37-38.6v-.5a22.861 22.861 0 0 0 6.3-6.7z"
                      fill="#202020"></path>
                    </svg>
                </a>
            </li>
            @endif

            @if($linkedin ?? false)
            <li>
                <a href="{{ $linkedin }}">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
                    aria-describedby="desc" role="img" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <title>Linkedin</title>
                      <desc>LinkedIn Logo in solid color.</desc>
                      <path data-name="layer1"
                      fill="#202020" d="M1.15 21.7h13V61h-13zm46.55-1.3c-5.7 0-9.1 2.1-12.7 6.7v-5.4H22V61h13.1V39.7c0-4.5 2.3-8.9 7.5-8.9s8.3 4.4 8.3 8.8V61H64V38.7c0-15.5-10.5-18.3-16.3-18.3zM7.7 2.6C3.4 2.6 0 5.7 0 9.5s3.4 6.9 7.7 6.9 7.7-3.1 7.7-6.9S12 2.6 7.7 2.6z"></path>
                    </svg>
                </a>
            </li>
            @endif

        </ul>

    </div>
    {{-- <div class="hamburger" id="hamburger">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
    </div> --}}
    <svg class="ham hamRotate ham8" id="hamburger" viewBox="0 0 100 100" width="60">
        <path
              class="line top"
              d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20" />
        <path
              class="line middle"
              d="m 30,50 h 40" />
        <path
              class="line bottom"
              d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20" />
      </svg>
</div>
<!-- End Header07 -->