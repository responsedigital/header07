class Header07 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {
        this.parent = this.getElementsByClassName('.header-07')[0];
        this.hamburger = this.querySelector('#hamburger');
    }

    connectedCallback () {
        this.initHeader07()
    }

    initHeader07 () {
        const { options } = this.props
        const config = {

        }

        this.hamburger.addEventListener('click', e => {
            this.hamburger.classList.toggle("active")
            this.classList.toggle("header-07--show")
        })
    }

}

window.customElements.define('fir-header-07', Header07, { extends: 'div' })
